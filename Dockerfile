FROM debian:stable-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
    okular \
    && apt clean
