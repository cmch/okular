* Dockerfile


C-u C-c C-v t


#+HEADER: :tangle Dockerfile
#+BEGIN_SRC sh

FROM debian:stable-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
    okular \
    && apt clean

#+END_SRC



* build at google vm

** build 


cd /tmp


# git clone https://cmch@bitbucket.org/cmch/okular.git


git clone --single-branch --branch debian https://cmch@bitbucket.org/cmch/okular.git


cd okular


time nice -10 \
docker build \
-t okular:debian \
.




# clean up okular directory


cd /tmp
rm -rf /tmp/okular



** results


[2019-07-24 Wed 03:49]

real    5m39.801s
user    0m1.364s
sys     0m0.844s


REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
okular              debian              ae0962f7d544        14 seconds ago      719MB


docker run --rm okular:debian apt list okular 


#  okular/stable,now 4:17.12.2-2.2 amd64 [installed]


* docker save load run

** docker save 

https://docs.docker.com/engine/reference/commandline/save/


#  $  docker save busybox > busybox.tar
#  $  docker save --output busybox.tar busybox


cd /tmp


docker save --output okular.tar okular:debian


ls -lh


** docker load


#  $  docker load --input fedora.tar


docker load --input /tmp/okular.tar


** docker run


f=/mnt/local/fonts/noto
s=/mnt/local/chrome.json

# cups, add proxy in comman line
docker run -it --rm \
    --name okular  \
    -v $f:/usr/share/fonts/noto \
    -v /etc/localtime:/etc/localtime:ro   \
    -v /tmp:/tmp \
    -v /mnt:/mnt \
    -e DISPLAY=$DISPLAY \
    --shm-size=1gb \
    -u 1000:1000 \
    silvavlis/okular





* versions

|                        | debian:stable-slim | alpine:edge          | alpine        |
|                        |                    |                      |               |
| [2019-07-24 Wed 00:39] |                    |                      | v3.10         |
|                        |                    |                      |               |
|------------------------+--------------------+----------------------+---------------|
| okular                 |                    |                      |               |
|                        |                    |                      |               |
| [2019-07-24 Wed 00:37] |      4:17.12.2-2.2 | 19.04.3-r0           | not available |
|                        |                    |                      |               |
| tag                    |                 17 | 19.04.3 build failed |               |


* repositories

C-u C-c C-v t


#+HEADER: :tangle repositories
#+BEGIN_SRC sh
@edge http://nl.alpinelinux.org/alpine/edge/main
@edgecommunity http://nl.alpinelinux.org/alpine/edge/community
@testing http://nl.alpinelinux.org/alpine/edge/testing
#+END_SRC

#+RESULTS:


* flows


* errors

** alpine

ERROR: unsatisfiable constraints:
  dbus-libs-1.12.16-r0:

The command '/bin/sh -c apk --no-cache add okular@testing' returned a non-zero code: 99

